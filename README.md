# pns-dashboard CL Package

A command-line package to add object of `log` type to AWS CloudWatch Dashboard Body for new SNS Platform application.

1. Install the dependency via:
    ```shell
    $ npm i --save-dev https://gitlab.com/NatalieWong/pns-dashboard-cli.git
    ```
2. In your project's `package.json`, add:
    ```json
        "script": {
            "<custom-commandName>": "pns-dashboard upd_util",
            "<custom-commandName-2>": "pns-dashboard add",
            ...
        }
    ```
3. Execute the command starting with `npm run <custom-commandName> -- ` and specify the following options:
    - `-s, --stage <stage-id>`
    - `-n, --app-name <name>`

    Example:
    
    `npm run update-util-dashboard -- -s dev -n my.org.test.app`

    OR

    `npm run update-util-dashboard -- --stage dev --app-name my.org.test.app`

    ---

    Execute the command starting with `npm run <custom-commandName-2> -- ` and specify the following options:
    - `-ac, --account-id <id>`
    - `-s, --stage <stage-id>`
    - `-n, --app-name <name>`
    - `-t, --apns-type <apns-type>`

    Example:
    
    `npm run update-platform-app-dashboard -- -ac 123456123456 -s dev -n my.org.test.app -t APNS`

    OR

    `npm run update-platform-app-dashboard -- --account-id 123456123456 --stage dev --app-name my.org.test.app --apns-type APNS`

***Troubleshooting***:
```shell
$ rm -rf .\node_modules\
$ npm i
```

---

## Dev Setup
### NPM Packages
```shell
$ npm i commander chalk
```

## Test locally
### Create symbolic link
```shell
$ npm link
```
#### Remove link
```shell
$ npm unlink
```
<!-- ```shell
$ npm install -g
```

### Checking
```shell
$ npm ls -g --depth=0 # list all globally installed Node.js modules
```

### Uninstall script
```shell
$ npm uninstall -g pns-dashboard-cli
``` -->

## Run
```shell
$ pns-dashboard add --account-id <id> --stage <stage-id> --app-name <name> --apns-type <apns-type>
```
Or
```shell
$ pns-dashboard add -ac <id> -s <stage-id> -n <name> -t <apns-type>
```
