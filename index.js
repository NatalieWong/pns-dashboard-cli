#!/usr/bin/env node

const chalk = require('chalk');
const program = require('commander');
const yaml = require('js-yaml');
const version = require('./package.json').version;
const fs = require('fs');

console.log(__dirname);

program.version(version);
program.command('add')
    .description('Add (and modify) objects of "log" type to CloudWatch Dashboard Body for new SNS Platform app')
    .option('-ac, --account-id <id>', 'set AWS account ID')
    .option('-s, --stage <stage-id>', 'set the stage of deployment')
    .option('-n, --app-name <name>', 'set name of the new SNS Platform app')
    .option('-t, --apns-type <apns-type>', 'set platform of the app')
    // .option('-f, --file <filename>', 'specify the JSON file name of the Dashboard body in the project')
    .action(add);
program.command('upd_util')
    .description('Add widgets to CloudWatch Utilization Dashboard Body for new SNS Platform app')
    .option('-s, --stage <stage-id>', 'set the stage of deployment')
    .option('-n, --app-name <name>', 'set name of the new SNS Platform app')
    .action(upd_util);

const args = process.argv;

if (args.length < 2) {
    console.log(chalk.red('No command is provided.\nPlease run "pns-dashboard upd_util" or "pns-dashboard add"'));
}

program.parse(args);

/**
 * Append log group at the beginning of the log widget query
 * @param {JSON} dashboardBody 
 * @param {string} title 
 * @param {string} logGrp 
 */
function updateWidget_direct(dashboardBody, title, logGrp) {
    const wgtIndex_direct = dashboardBody.widgets.findIndex(widget => {
        return widget.properties.title == `${title}`;
    });
    tempQuery = dashboardBody.widgets[wgtIndex_direct].properties.query;
    // add log group
    tempQuery = `SOURCE '${logGrp}' | ` + tempQuery;

    dashboardBody.widgets[wgtIndex_direct].properties.query = tempQuery;

    console.log('Updated widget query of', chalk.green(`${title}`));
    // debug
    // console.debug(dashboardBody.widgets[wgtIndex_direct].properties.query);

    return dashboardBody;
}

/**
 * Append log group at the beginning of the log widget query
 * and modify the filter command.
 * 
 * Applied to both Overview & app-specific Dashboard updates.
 * @param {JSON} dashboardBody 
 * @param {string} title 
 * @param {string} logGrp 
 * @param {string} appDeliveryFailureTopic 
 * @param {boolean} isOverview 
 */
function updateWidget_topic(dashboardBody, title, logGrp, appDeliveryFailureTopic, isOverview) {
    const wgtIndex_broadcast = dashboardBody.widgets.findIndex(widget => {
        return widget.properties.title == `${title}`;
    });
    tempQuery = dashboardBody.widgets[wgtIndex_broadcast].properties.query;
    // add log group
    tempQuery = `SOURCE '${logGrp}' | ` + tempQuery;
    
    // update query filter command
    let cmdStartPos_stats = tempQuery.lastIndexOf('stats');
    if (isOverview) {
        // for Overview Dashboard
        if (tempQuery.includes(' and (notification.topicArn not like ')) {
            // update dashboard for the 2nd time or afterwards
            cmdStartPos_stats -= 4;
            tempQuery = tempQuery.slice(0, cmdStartPos_stats) + ` or notification.topicArn not like \"${appDeliveryFailureTopic}\"` + tempQuery.slice(cmdStartPos_stats);
        } else {
            // update dashboard for the 1st time
            cmdStartPos_stats -= 3;
            tempQuery = tempQuery.slice(0, cmdStartPos_stats) + ` and (notification.topicArn not like \"${appDeliveryFailureTopic}\")` + tempQuery.slice(cmdStartPos_stats);
        }
    } else {
        // for app-specific Dashboards
        
        // the update of filter command should be executed only ONCE
        if (!tempQuery.includes("notification.topicArn not like")) {
            if (cmdStartPos_stats == -1) {
                // for failure log widget query
                // (displayed in TABLE located in the middle to bottom of the dashboard)
                tempQuery += ` and notification.topicArn not like \"${appDeliveryFailureTopic}\"`;
            } else {
                cmdStartPos_stats -= 3;
                tempQuery = tempQuery.slice(0, cmdStartPos_stats) + ` and notification.topicArn not like \"${appDeliveryFailureTopic}\"` + tempQuery.slice(cmdStartPos_stats);
            }
        }
    }
    dashboardBody.widgets[wgtIndex_broadcast].properties.query = tempQuery;

    console.log('Updated widget query of', chalk.green(`${title}`));
    // debug
    // console.debug(dashboardBody.widgets[wgtIndex_broadcast].properties.query);

    return dashboardBody;
}

/**
 * Modify the platform applications' Overview Dashboard
 * by inserting log groups of new platform application and
 * modifying the query of the log widgets.
 * @param {string} appName 
 * @param {Array} pushNotiPlatforms 
 * @param {Array} logGroups 
 * @param {Array} logGroups_failure 
 * @param {Array} appBroadcastTopics 
 * @param {string} appDeliveryFailureTopic 
 * @param {string} file in JSON format defining the Overview Dashboard body
 */
function updateOverviewDashboard(
    appName, appNameWithStageId,
    pushNotiPlatforms,
    logGroups, logGroups_failure,
    appBroadcastTopics, appDeliveryFailureTopic,
    file) {
    // constants
    // widget titles (for widget searching)
    let widgetTitles_apns = [];
    widgetTitles_apns.push("APNS - Direct Delivery - Success Count");
    widgetTitles_apns.push("APNS - Topic message Delivery - Success Count");
    widgetTitles_apns.push("APNS - Direct Delivery - Failure Count");
    widgetTitles_apns.push("APNS - Topic message Delivery - Failure Count");

    let widgetTitles_gcm = [];
    widgetTitles_gcm.push("GCM - Direct Delivery - Success Count");
    widgetTitles_gcm.push("GCM - Topic message Delivery - Success Count");
    widgetTitles_gcm.push("GCM - Direct Delivery - Failure Count");
    widgetTitles_gcm.push("GCM - Topic message Delivery - Failure Count");
    
    // array index pointer for
    // logGroups, logGroups_failure, appBroadcastTopics
    let pushNotiPlatformIndex = 0;

    try {
        /**
         * read from JSON file
         */
        const rawDataStr = fs.readFileSync(file, 'utf-8');
        let dashboardBody = JSON.parse(rawDataStr);
        // console.debug(dashboardBody);

        console.log('\n'+chalk.bold('Start updating PNS Overview Dashboard Body'));

        // initialize variables
        let textProps, sns_app_md;
        // handle repeated execution to 
        // check if the dashboard has already been updated
        let isUpdated = false;

        // update dashboard FOR EACH push notification platform
        // execute twice: one for APNS (or VOIP or SANDBOX ...), another for GCM
        pushNotiPlatforms.forEach(pushNotiPlatform => {
            console.log(chalk.grey(`... updating ${pushNotiPlatform} related widgets`));
            /**
             * 1. update markdown string
             */
            textProps = dashboardBody.widgets[0].properties; // { markdown: '...' }
            sns_app_md = `* ${appNameWithStageId} (${pushNotiPlatform})\n`;
            // handle repeated execution to 
            // check if the dashboard has already been updated
            if (!textProps.markdown.includes(sns_app_md)) {
                textProps.markdown += sns_app_md;
                dashboardBody.widgets[0].properties = {
                    "markdown": textProps.markdown
                };
                console.log('Updated', chalk.green('Markdown'));
                // console.debug(dashboardBody.widgets[0].properties.markdown);
            } else {
                // terminate the execution of the function
                // if the dashboard has already been updated
                console.log('\n'+chalk.yellow('WARN! Already updated for', chalk.bold(appNameWithStageId), `(${chalk.bold(pushNotiPlatform)})`));
                isUpdated = true;
                return;
            }

            /**
             * 2. update widgets of CLW groups
             */
            if (pushNotiPlatformIndex == 0) {
                // APNS
                // Direct Delivery
                // - Success Count
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_apns[0], logGroups[pushNotiPlatformIndex]);
                // - Failure Count
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_apns[2], logGroups_failure[pushNotiPlatformIndex]);
                
                // Topic message Delivery
                // - Success Count
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_apns[1], logGroups[pushNotiPlatformIndex], appDeliveryFailureTopic, true);
                // - Failure Count
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_apns[3], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, true);
            } else {
                // GCM
                // Direct Delivery
                // - Success Count
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_gcm[0], logGroups[pushNotiPlatformIndex]);
                // - Failure Count
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_gcm[2], logGroups_failure[pushNotiPlatformIndex]);

                // Topic message Delivery
                // - Success Count
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_gcm[1], logGroups[pushNotiPlatformIndex], appDeliveryFailureTopic, true);
                // - Failure Count
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_gcm[3], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, true);
            }

            console.log(chalk.bold('Updated PNS Overview Dashboard Body')+' for '+appName+' ('+pushNotiPlatform+')');

            // update dashboard widgets for the next push notification platform
            pushNotiPlatformIndex++;
        });

        if (!isUpdated) {
            /**
             * write to JSON file
             */
            const updatedDataStr = JSON.stringify(dashboardBody, null, 4);
            fs.writeFileSync(file, updatedDataStr);

            console.log(chalk.bold('Finished updating PNS Overview Dashboard Body'));
        }
    } catch (err) {
        console.log(chalk.red('ERR! Failed to R/W JSON file')+' for Overview Dashboard body updates');
        console.error(err);
    }
}

/**
 * Create a new folder, JavaScript and JSON files for new platform application.
 * 
 * Modify the YAML file for Dashboard creation using AWS CloudFormation.
 * @param {string} appName 
 * @param {string} appName_tail 
 * @param {string} stage 
 * @param {Array} pushNotiPlatforms 
 * @param {Array} logGroups 
 * @param {Array} logGroups_failure 
 * @param {Array} appBroadcastTopics 
 * @param {string} appDeliveryFailureTopic 
 * @param {Array} templateFiles 
 * @param {string} appDashboardDir 
 * @param {Array} appDashboardFiles 
 * @param {string} appDashboardsYaml 
 */
function createAppSpecificDashboard(
    appName, appName_tail, stage,
    pushNotiPlatforms,
    logGroups, logGroups_failure,
    appBroadcastTopics, appDeliveryFailureTopic,
    templateFiles, appDashboardDir, appDashboardFiles, appDashboardsYaml) {
    // constants
    // const bin_val = '1m'; // m - minutes, h - hour
    const stage_1stCharUpperCase = `${stage[0].toUpperCase()+stage.slice(1)}`;
    const js_functionName = `${appName_tail+stage_1stCharUpperCase}DashboardBody`;

    // widget titles (for widget searching)
    let widgetTitles_apns = [];
    widgetTitles_apns.push("APNS - Count - Direct Delivery");
    widgetTitles_apns.push("APNS - Count - Direct Delivery (table view)");
    widgetTitles_apns.push("APNS - Count - Topic message Delivery");
    widgetTitles_apns.push("APNS - Count - Topic message Delivery (table view)");
    widgetTitles_apns.push("APNS - Failure Count - Direct Delivery");
    widgetTitles_apns.push("APNS - Failure Log - Direct Delivery");
    widgetTitles_apns.push("APNS - Failure Count - Topic message Delivery");
    widgetTitles_apns.push("APNS - Failure Log - Topic message Delivery");

    let widgetTitles_gcm = [];
    widgetTitles_gcm.push("GCM - Count - Direct Delivery");
    widgetTitles_gcm.push("GCM - Count - Direct Delivery (table view)");
    widgetTitles_gcm.push("GCM - Count - Topic message Delivery");
    widgetTitles_gcm.push("GCM - Count - Topic message Delivery (table view)");
    widgetTitles_gcm.push("GCM - Failure Count - Direct Delivery");
    widgetTitles_gcm.push("GCM - Failure Log - Direct Delivery");
    widgetTitles_gcm.push("GCM - Failure Count - Topic message Delivery");
    widgetTitles_gcm.push("GCM - Failure Log - Topic message Delivery");


    // create an app-specific directory
    //   to hold all the related files
    try {
        // error handling for deployment using Jenkins (pipeline)
        if (fs.existsSync(appDashboardDir)) {
            fs.rmdirSync(appDashboardDir, { recursive: true });
            console.log('\n'+chalk.yellow(`Removed existing app-specific directory: ${appDashboardDir}`));
        }
        fs.mkdirSync(appDashboardDir);
        console.log('\n'+chalk.bold(`Created an app-specific directory: ${appDashboardDir}`));
    } catch (err) {
        console.log(chalk.red(`ERR! Failed to create an app-specific directory: ${appDashboardDir}`));
        console.error(err);
        return;
    }


    console.log(chalk.bold('\n'+'Start creating PNS platform application Dashboard Body')+' for '+appName);
    // - 2.1 read & modify dummy JSON file (template)
    try {
        const templateDataStr = fs.readFileSync(templateFiles[0], 'utf-8');
        let dashboardBody = JSON.parse(templateDataStr);

        // - 2.2.1 update markdown in text widget
        let mdStr = dashboardBody.widgets[0].properties.markdown;
        mdStr = mdStr.replace(/{dummyAppName}/g, appName);
        mdStr = mdStr.replace(/{env}/g, stage);
        mdStr += `* ${logGroups[0]}\n* ${logGroups_failure[0]}\n* ${logGroups[1]}\n* ${logGroups_failure[1]}\n`;
        dashboardBody.widgets[0].properties.markdown = mdStr;

        console.log('Created', chalk.green('Markdown'));

        // - 2.2.2 update log widget query
        
        // array index pointer for
        // logGroups, logGroups_failure, appBroadcastTopics
        let pushNotiPlatformIndex = 0;

        // update dashboard FOR EACH push notification platform
        // execute twice: one for APNS (or VOIP or SANDBOX ...), another for GCM
        while (pushNotiPlatformIndex < pushNotiPlatforms.length) {
            console.log(chalk.grey(`... creating ${pushNotiPlatforms[pushNotiPlatformIndex]} related widgets`));

            if (pushNotiPlatformIndex == 0) {
                // APNS
                // - Count - Direct Delivery
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_apns[0], logGroups_failure[pushNotiPlatformIndex]);
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_apns[0], logGroups[pushNotiPlatformIndex]);
                // - Count - Direct Delivery (table view)
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_apns[1], logGroups_failure[pushNotiPlatformIndex]);
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_apns[1], logGroups[pushNotiPlatformIndex]);
                // - Failure Count - Direct Delivery
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_apns[4], logGroups_failure[pushNotiPlatformIndex]);
                // - Failure Log - Direct Delivery
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_apns[5], logGroups_failure[pushNotiPlatformIndex]);

                // - Count - Topic message Delivery
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_apns[2], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_apns[2], logGroups[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                // - Count - Topic message Delivery (table view)
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_apns[3], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_apns[3], logGroups[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                // - Failure Count - Topic message Delivery
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_apns[6], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                // - Failure Log - Topic message Delivery
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_apns[7], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
            } else {
                // GCM
                // - Count - Direct Delivery
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_gcm[0], logGroups_failure[pushNotiPlatformIndex]);
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_gcm[0], logGroups[pushNotiPlatformIndex]);
                // - Count - Direct Delivery (table view)
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_gcm[1], logGroups_failure[pushNotiPlatformIndex]);
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_gcm[1], logGroups[pushNotiPlatformIndex]);
                // - Failure Count - Direct Delivery
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_gcm[4], logGroups_failure[pushNotiPlatformIndex]);
                // - Failure Log - Direct Delivery
                dashboardBody = updateWidget_direct(dashboardBody, widgetTitles_gcm[5], logGroups_failure[pushNotiPlatformIndex]);

                // - Count - Topic message Delivery
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_gcm[2], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_gcm[2], logGroups[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                // - Count - Topic message Delivery (table view)
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_gcm[3], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_gcm[3], logGroups[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                // - Failure Count - Topic message Delivery
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_gcm[6], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
                // - Failure Log - Topic message Delivery
                dashboardBody = updateWidget_topic(dashboardBody, widgetTitles_gcm[7], logGroups_failure[pushNotiPlatformIndex], appDeliveryFailureTopic, false);
            }
            // update dashboard widgets for the next push notification platform
            pushNotiPlatformIndex++;
        }

        // - 2.3 create a new JSON file
        const updatedDataStr = JSON.stringify(dashboardBody, null, 4);
        fs.writeFileSync(appDashboardFiles[0], updatedDataStr);

        console.log(chalk.bold(`Created a JSON file: ${appDashboardFiles[0]}`));
    } catch (err) {
        console.log(chalk.red('ERR! Failed to R/W JSON file')+' for platform application Dashboard body updates');
        console.error(err);
        return;
    }


    try {
        // - 3.1. read & modify dummy JS file (template)
        let rawDataStr = fs.readFileSync(templateFiles[1], 'utf-8');
        rawDataStr = rawDataStr.replace(/dummyAppName/g, appName_tail);
        rawDataStr = rawDataStr.replace(/Env/g, stage_1stCharUpperCase);
        rawDataStr = rawDataStr.replace(/env/g, stage);

        // - 3.2 create a new JS file
        fs.writeFileSync(appDashboardFiles[1], rawDataStr);

        console.log('\n'+chalk.bold(`Created a JavaScript file: ${appDashboardFiles[1]}`));
    } catch (err) {
        console.log(chalk.red('ERR! Failed to R/W JavaScript files')+' for platform application Dashboard body updates');
        console.error(err);
        return;
    }
    

    // - 4. read & modify yml file: add a new dashboard resource
    try {
        const fileContents = fs.readFileSync(appDashboardsYaml, 'utf-8');
        let yml_cf_resrc = yaml.load(fileContents); // return a JSON object
        // console.debug(yml_cf_resrc);


        // constants (names)
        const cf_resrc_name = `${appName_tail[0].toUpperCase()+appName_tail.slice(1)+stage_1stCharUpperCase}Dashboard`;
        const new_cf_resrc_type = `"Type":"AWS::CloudWatch::Dashboard"`;
        const new_cf_resrc_dashboardName = `"DashboardName":"pns-pushnoti-delivery-${appName_tail}-${stage}"`;
        const new_cf_resrc_dashboardBody = `"DashboardBody":"\${file(${appDashboardFiles[1]}):${js_functionName}}"`;
        const new_cf_resrc_properties = `"Properties":{${new_cf_resrc_dashboardName},${new_cf_resrc_dashboardBody}}`;
        const new_cf_resrc = `"${cf_resrc_name}":{${new_cf_resrc_type},${new_cf_resrc_properties}}`;


        // - 4.2.1. convert the JSON object to a string
        let yml_cf_resrc_str = JSON.stringify(yml_cf_resrc);
        // - 4.2.2. add `,"xxx":{...}` just before }}
        const lastCurlyBracketsPos = yml_cf_resrc_str.length - 2;
        const yml_cf_resrc_str_head = yml_cf_resrc_str.slice(0, lastCurlyBracketsPos);
        const yml_cf_resrc_str_tail = yml_cf_resrc_str.slice(lastCurlyBracketsPos);
        // - 4.2.3. add new property if NOT exists
        if (yml_cf_resrc_str.includes(new_cf_resrc)) return;
        yml_cf_resrc_str = `${yml_cf_resrc_str_head},${new_cf_resrc}${yml_cf_resrc_str_tail}`;
        // - 4.2.4. convert the string back to a JSON object
        yml_cf_resrc = JSON.parse(yml_cf_resrc_str);

        // - 4.3 overwrite the original YAML file of resources
        let yml_cf_resrc_str_updated = yaml.dump(yml_cf_resrc);
        fs.writeFileSync(appDashboardsYaml, yml_cf_resrc_str_updated, 'utf-8');

        console.log('\n'+chalk.bold(`Updated YAML file: ${appDashboardsYaml}`)+` for ${appName}-${stage} Dashboard creation`);
    } catch (err) {
        console.log(chalk.red('ERR! Failed to R/W YAML file')+' for platform application Dashboard body updates');
        console.error(err);
    }
}

/**
 * Modify the core-app Utilization Dashboard body
 * by adding log widgets
 * for a new platform application
 * @param {string} appName 
 * @param {string} stage 
 * @param {string} file in JSON format defining the Utilization Dashboard body
 */
function updateUtilizationDashboard(appName, stage, file) {
    // constants
    const aws_region_lambdaFunctions = "ap-east-1";
    const widget_titles = [
        `${appName}`,
        `${appName} - API call - Success and Failure Count`,
        `${appName} - Successful API call - Status Count`
    ];
    const lambdaFunctionLogGroupNames = [
        `/aws/lambda/pns-pushnoti-core-app-${stage}-register`,
        `/aws/lambda/pns-pushnoti-core-app-${stage}-registerWithTarget`,
        `/aws/lambda/pns-pushnoti-core-app-${stage}-retrieveRegistration`,
        `/aws/lambda/pns-pushnoti-core-app-${stage}-updateRegistration`,
        `/aws/lambda/pns-pushnoti-core-app-${stage}-deregisterTarget`,
        `/aws/lambda/pns-pushnoti-core-app-${stage}-deregister`,
        `/aws/lambda/pns-pushnoti-core-app-${stage}-untarget`,
        `/aws/lambda/pns-pushnoti-core-app-${stage}-broadcast`,
        `/aws/lambda/pns-pushnoti-core-app-${stage}-sendNotification`
    ];
    const query_fields_head = "fields APPLICATION_ID, substr(HOST_NAME, 26) as functionName, LOG_TYPE";
    const query_filter_head = `filter ispresent(APPLICATION_ID) and APPLICATION_ID like \"${appName}\" and ispresent(HOST_NAME) and ispresent(LOG_TYPE)`;
    const query_stats = "stats count(*) as Count by functionName";
    const query_sort = "sort functionName desc";

    let query_logGroups = "";
    lambdaFunctionLogGroupNames.forEach(logGrpName => {
        query_logGroups += `SOURCE '${logGrpName}' | `;
    });

    const query_pieChartView = `${query_logGroups}${query_fields_head}\n| ${query_filter_head} and LOG_TYPE like \"INFO\"\n| ${query_stats}\n| ${query_sort}`;
    const query_tableView_1 = `${query_logGroups}${query_fields_head}, STATUS\n| ${query_filter_head} and LOG_TYPE like \"INFO\" and ispresent(STATUS)\n| ${query_stats}, STATUS\n| ${query_sort}`;
    const query_tableView_2 = `${query_logGroups}${query_fields_head}, DESCRIPTION, STATUS\n| ${query_filter_head} and ispresent(DESCRIPTION) and ispresent(STATUS)\n| ${query_stats}, LOG_TYPE, DESCRIPTION, STATUS\n| ${query_sort}`;

    const logWidgetObj_pieChart = {
        "type": "log",
        "width": 12,
        "properties": {
            "region": `${aws_region_lambdaFunctions}`,
            "title": `${widget_titles[0]}`,
            "query": `${query_pieChartView}`,
            "view": "pie"
        }
    };
    const logWidgetObj_table_1 = {
        "type": "log",
        "width": 12,
        "properties": {
            "region": `${aws_region_lambdaFunctions}`,
            "title": `${widget_titles[1]}`,
            "query": `${query_tableView_1}`
        }
    };
    const logWidgetObj_table_2 = {
        "type": "log",
        "width": 24,
        "properties": {
            "region": `${aws_region_lambdaFunctions}`,
            "title": `${widget_titles[2]}`,
            "query": `${query_tableView_2}`
        }
    };

    
    try {
        /**
         * read from JSON file
         */
        const rawDataStr = fs.readFileSync(file, 'utf-8');
        let dashboardBody = JSON.parse(rawDataStr);
        // console.debug(dashboardBody);

        console.log(chalk.bold('\n'+`Start updating PNS core-app-${stage} Utilization Dashboard Body`)+' for '+appName);
        // handle repeated execution
        const sns_app_md = `* ${appName}-${stage}\n`;
        if (dashboardBody.widgets[2].properties.markdown.includes(sns_app_md)) {
            console.log('\n'+chalk.yellow(`WARN! Already updated for`, chalk.bold(`${appName}-${stage}`)));
            return;
        }
        // update text widget
        dashboardBody.widgets[2].properties.markdown += sns_app_md;
        console.log('Updated', chalk.green('Markdown'));

        // add widgets for the new SNS platform app

        // specify the existing app which should be staying at the bottom of the dashboard
        // --> hk.org.ha.testpushcert
        const appAtBottom = 'hk.org.ha.testpushcert';
        const wgtIndex_appAtBottom = dashboardBody.widgets.findIndex(widget => {
            return widget.properties.title == `${appAtBottom}`;
        });
        if (wgtIndex_appAtBottom == -1) {
            // insert widgets of the new app at the bottom
            dashboardBody.widgets.push(logWidgetObj_pieChart);
            dashboardBody.widgets.push(logWidgetObj_table_1);
            dashboardBody.widgets.push(logWidgetObj_table_2);
        } else {
            // insert widgets of the new app at the bottom
            // right above the first widget of 'hk.org.ha.testpushcert'
            dashboardBody.widgets.splice(
                wgtIndex_appAtBottom, /* starting pos for insertion */
                0, /* none of the item should be removed */ 
                logWidgetObj_pieChart, 
                logWidgetObj_table_1, 
                logWidgetObj_table_2
            );
        }
        console.log('Added log widget for', chalk.green(`${widget_titles[0]}`), '(pie chart)');
        console.log('Added log widget for', chalk.green(`${widget_titles[1]}`));
        console.log('Added log widget for', chalk.green(`${widget_titles[2]}`));

        /**
         * write to JSON file
         */
        const updatedDataStr = JSON.stringify(dashboardBody, null, 4);
        fs.writeFileSync(file, updatedDataStr);

        console.log(chalk.bold(`Updated PNS core-app-${stage} Utilization Dashboard Body`));
    } catch (err) {
        console.log(chalk.red(`ERR! Failed to R/W JSON file`)+` for core-app-${stage} Utilization Dashboard body updates`);
        console.error(err);
    }
}

async function add(command) {
    // ATTENTION! var name must be the same as the boolean flag
    const { accountId, stage, appName, apnsType/*, file*/ } = command;
    // console.debug(command);
    // console.debug(appName, appPlatform, accountId, file);
    
    // ERROR handling of cmd args
    if (!accountId || !stage || !appName || !apnsType /*|| !file*/) {
        if (!accountId) {
            console.log(chalk.red('ERR! You must specify the AWS AccountId'));
            console.log('usage: pns-dashboard add -ac <id>');
        }
        if (!stage) {
            console.log(chalk.red('ERR! You must specify the stage of deployment'));
            console.log('usage: pns-dashboard add -s <stage-id>');
        }
        if (!appName) {
            console.log(chalk.red('ERR! You must specify the name of the new SNS Platform app'));
            console.log('usage: pns-dashboard add -n <name>');
        }
        if (!apnsType) {
            console.log(chalk.red('ERR! You must specify the APNS type of the new SNS Platform app'));
            console.log('usage: pns-dashboard add -t <apns-type>');
        }
        // if (!file) {
        //     console.log(chalk.red('ERR! You must specify the JSON file name of the Dashboard body in the project'));
        //     console.log('usage: pns-dashboard add -f <filename>');
        // }
    } else {
        // constants
        const AWS_SNS_REGION = 'ap-southeast-1';
        const appName_tail = appName.substr(appName.lastIndexOf('.') + 1);
        const appName_forSNSTopic = `${appName.replace(/\./g, "_")}`;
        const appNameWithStageId = `${appName}-${stage}`;
        const appDeliveryFailureTopic = `${appName_forSNSTopic}-${stage}-delivery-failure`;

        // update dashboard FOR EACH push notification platform
        let pushNotiPlatforms = [];
        pushNotiPlatforms.push(apnsType);
        pushNotiPlatforms.push('GCM');
        
        // build the CloudWatch log (CWL) group & broadcast topic name dynamically
        let logGroups = [], logGroups_failure = [], appBroadcastTopics = [];

        pushNotiPlatforms.forEach(platform => {
            logGroups.push(`sns/${AWS_SNS_REGION}/${accountId}/app/${platform}/${appNameWithStageId}`);
            logGroups_failure.push(`sns/${AWS_SNS_REGION}/${accountId}/app/${platform}/${appNameWithStageId}/Failure`);
            appBroadcastTopics.push(`${appName_forSNSTopic}-${platform}-${stage}`);
        });

        // debug
        // console.debug(logGroups);
        // console.debug(logGroups_failure);
        // console.debug(appBroadcastTopics);
        
        // files to be modified & created
        // const coreAppUtilizationDashboardFile = `core-app/${stage}/core-app-utilization-dashboard-body.json`; // moved to upd_util cmd
        const appsOverviewDashboardFile = `platform-app/${stage}/delivery-overview-dashboard-body.json`;
        const appDashboardsYaml = `resources/${stage}/platform-app-${stage}-dashboards.yml`;

        // dummy JS & JSON files for replication
        let templateFiles = [];
        templateFiles.push('platform-app/template-for-updates/dummy-env-dashboard-body.json');
        templateFiles.push('platform-app/template-for-updates/dummy-env-dashboard.js');
        
        const appDashboardDir = `platform-app/${stage}/${appName_tail}`;
        let appDashboardFiles = [];
        appDashboardFiles.push(`${appDashboardDir}/${appName_tail}-${stage}-dashboard-body.json`);
        appDashboardFiles.push(`${appDashboardDir}/${appName_tail}-${stage}-dashboard.js`);

        // debug
        // console.debug(files);

        // update and/or create dashboards
        // updateUtilizationDashboard(appName, stage, coreAppUtilizationDashboardFile); // moved to upd_util cmd
        updateOverviewDashboard(appName, appNameWithStageId,
            pushNotiPlatforms,
            logGroups, logGroups_failure,
            appBroadcastTopics, appDeliveryFailureTopic,
            appsOverviewDashboardFile);
        createAppSpecificDashboard(appName, appName_tail, stage,
            pushNotiPlatforms,
            logGroups, logGroups_failure,
            appBroadcastTopics, appDeliveryFailureTopic,
            templateFiles, appDashboardDir, appDashboardFiles, appDashboardsYaml);
    }
}

function upd_util(command) {
    // ATTENTION! var name must be the same as the boolean flag
    const { stage, appName } = command;
    
    // ERROR handling of cmd args
    if (!stage || !appName) {
        if (!stage) {
            console.log(chalk.red('ERR! You must specify the stage of deployment'));
            console.log('usage: pns-dashboard upd_util -s <stage-id>');
        }
        if (!appName) {
            console.log(chalk.red('ERR! You must specify the name of the new SNS Platform app'));
            console.log('usage: pns-dashboard upd_util -n <name>');
        }
    } else {
        const coreAppUtilizationDashboardFile = `core-app/${stage}/core-app-utilization-dashboard-body.json`;
        updateUtilizationDashboard(appName, stage, coreAppUtilizationDashboardFile);
    }
}
